tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

// pomysłem na zagnieżdżanie ifów i rozpoznawanie poszczególnych etykiet jest zastosowanie zmiennej przechowującej poziom zagnieżdzenia
@members {
  Integer nest_level = 0;
}

prog    : (e+=expr | d+=decl)* -> program(name = { $e }, deklaracje = { $d });

decl  :
        ^(VAR i1=ID) { globals.newSymbol($ID.text); } -> declare(name = { $ID.text })
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(val1 = { $e1.st }, val2 = { $e2.st })
        | ^(MINUS e1=expr e2=expr) -> subtract(val1 = { $e1.st }, val2 = { $e2.st })
        | ^(MUL   e1=expr e2=expr) -> multiply(val1 = { $e1.st }, val2 = { $e2.st })
        | ^(DIV   e1=expr e2=expr) -> divide(val1 = { $e1.st }, val2 = { $e2.st })
        | ^(PODST i1=ID   e2=expr) -> assign(name = { $i1.text }, value = { $e2.st })
        | INT                      -> int(val = { $INT.text })
        | ID                       -> id(name = { $ID.text })
        | ^(IF e1=expr e2=expr e3=expr?) {nest_level++;} -> if(condition = { $e1.st }, on_true = { $e2.st }, on_false = { $e3.st }, nest_level = { nest_level.toString() })
    ;
    